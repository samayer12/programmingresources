# Contributing

---

Please open a new issue to clarify any questions, or post in the `General discussion issue`.

All guidelines for adding new software to the list are listed in `PULL_REQUEST_TEMPLATE.md`.

#### Other recommendations:

* To add a new entry, edit the `README.md` file through Gitlab's web interface or a text editor, and send a [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/).
* A script to help you format new entries is available at (it requires make to be installed): git clone/download and enter the repository, run `make add` and follow the instructions.
* A website to help you format new entries is available at https://n8225.github.io/
* The list of contributors can be updated with `make contrib`.
* Software with no development activity for 6-12 months may be removed from the list.
* Don't know where to start? Check issues labeled `help wanted` and `fix`.
