# ProgrammingResources

A list of resources I've found helpful in my programming experience.

[[_TOC_]]

### Converters & Decoders

- [crontab guru](https://crontab.guru/#47_6_*_*_6) - Quick & simple editor for cron schedule expressions, by [Cronitor](https://cronitor.io/cron-job-monitoring).
- [Epoch & Unix Timestamps](https://www.epochconverter.com/) - Convert epoch to human-readable date and vice versa.
- [Hexadecimal Packet Decoder](https://hpd.gasmi.net/) - Hexadecimal goes in, packets go out. Created by [Salim Gasmi](https://gasmi.net/).
- [Hex:IP](https://www.browserling.com/tools/hex-to-ip) - Hexadecimal goes in, IP addresses go out.
- [IP Subnet Calculator](https://www.calculator.net/ip-subnet-calculator.html) - Calculate possible network addresses, usable host ranges, subnet mask, and IP class, and more!
- [OUI Lookup](https://www.wireshark.org/tools/oui-lookup.html) - Associate OUIs and other MAC prefixes.
- [Timezones](https://www.timeanddate.com/worldclock/converter.html) - Time zone conversions taking into account Daylight Saving Time (DST), local time zone and accepts present, past, or future dates.
- [Unicode tools](https://onlineunicodetools.com/?msg11) - Do tons of different conversions and operations on unicode text.

### Data Science

- [Data Science Cookbook](https://datasciencecookbook.github.io/) - A compendium of links that discuss data-science concepts, kinda like this repo!
- [I Don't Like Notebooks](https://www.youtube.com/watch?v=7jiPeIFXb6U) - Some reasons why Jupyter Notebooks aren't the best, also available as [text](https://datapastry.com/blog/why-i-dont-use-jupyter-notebooks-and-you-shouldnt-either/).
- [R for Data Science](https://r4ds.had.co.nz/) - How to get your data into R, get it into the most useful structure, transform it, visualise it and model it.


### Documentation

- [Markdown](https://www.markdownguide.org/getting-started/) - A lightweight markup language that you can use to add formatting elements to plaintext text documents.
- [Mermaid](https://mermaid-js.github.io/mermaid/#/) - Diagramming and charting tool that renders Markdown-inspired text definitions to create and modify diagrams dynamically.
- [Pandoc](https://pandoc.org/) - If you need to convert files from one markup format into another, pandoc is your swiss-army knife.
- [readthedocs.io](https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html) - A system for writing technical documentation with Sphinx.
- [reveal.js](https://revealjs.com/) - A enables anyone with a web browser to create fully-featured and beautiful presentations for free (.ppt alternative).

### Git

- [.gitignore generator](https://www.toptal.com/developers/gitignore) - Auto-generate .gitignore files.
- [Atomic Commits](https://www.freshconsulting.com/insights/blog/atomic-commits/) - An "atomic" change revolves around one task or one fix.
- [Enforcing Commit Message style](https://auscunningham.medium.com/enforcing-git-commit-message-style-b86a45380b0f) - A script to enforce commit messages of a certain style as a git hook.
- [Git Fix Um](https://sethrobertson.github.io/GitFixUm/fixup.html) - A choose-your-own-adventure style guide to troubleshooting git issues.
- [How to write a commit message](https://chris.beams.io/posts/git-commit/) - Play nice with others and learn what makes for a good commit message.
- [Learn Git Branching](https://learngitbranching.js.org/) - An interactive way to visualize what git commands do to your repository.
- [Submodules](https://www.atlassian.com/git/tutorials/git-submodule) - Git submodules allow you to keep a git repository as a subdirectory of another git repository.

### Test-Driven Development

- [Let's Play: TDD](http://www.jamesshore.com/v2/projects/lets-play-tdd) - A screencast series featuring Java, test-driven development, and evolutionary design.
- [Mocks Aren't Stubs](https://martinfowler.com/articles/mocksArentStubs.html) - How mock objects work, how they encourage testing based on behavior verification, and they enable a different style of testing.
- [PawCast with GeePaw Hill](https://open.spotify.com/show/6rte6liF1WtEW3Pxrr2E9y) - Michael "GeePaw" Hill talks about his experiences with Test-Driven Development in 10-minute episodes.
- [TDD & the Lump of Coding Fallacy](https://www.geepawhill.org/2018/04/14/tdd-the-lump-of-coding-fallacy/) - Writing tests for your code doesn't slow you down in the long-run.
- [The Test-Double Rule of Thumb](https://web.archive.org/web/20210116023502/http://engineering.pivotal.io/post/the-test-double-rule-of-thumb/) - Use doubles to stand in for collaborating components in your tests.
- [The Art of Agile Development: TDD](https://www.jamesshore.com/v2/books/aoad1/test_driven_development) - TDD slashes the rate of defects and can improve your design, document public interfaces, and guard against future mistakes.
- [Workflows for Refactoring](https://martinfowler.com/articles/workflowsOfRefactoring/) - Learn to integrate refactoring more deeply into your work to gain a better code-base that makes adding new features easier.

### Python

* [Lectures from a Core developer](https://pyvideo.org/speaker/raymond-hettinger.html) - Raymond Hettinger puts together lots of engaging content that shares his experiences in Python with the rest of us.
* [Test Doubles with Python](https://www.youtube.com/watch?v=h75UJmzXz6k) - A video Sam Mayer made on Test-Doubles with Python.
* [Unit Testing with Python](https://www.youtube.com/watch?v=tzEtwwoWQxg) - A video Sam Mayer made on Unit testing with Python.