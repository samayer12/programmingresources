Thank you for taking the time to work on a MR for Programming Resources!

To ensure your MR is dealt with swiftly please check the following:

- [ ] Submit one item per merge request. This eases reviewing and speeds up inclusion.
- [ ] Format your submission as follows:
  Do not add a duplicate `Source code` link if it is the same as the main link.
  Keep the short description under 250 characters and use [sentence case](https://en.wikipedia.org/wiki/Letter_case#Sentence_case)
  for it, even if the project's webpage or readme uses another capitalisation
  such as title case, all caps, small caps or all lowercase.
- [ ] Additions are inserted preserving alphabetical order.
- [ ] You have searched the repository for any relevant [issues](https://gitlab.com/samayer12/programmingresources/-/issues) or [MRs](https://gitlab.com/samayer12/programmingresources/-/merge_requests), including closed ones.
- [ ] Any category you are creating has the minimum requirement of 3 items.
  If not, your addition may be inserted into `Misc/Other`.
- [ ] The pull request title is informative, unlike "Update README.md".
  Suggested titles: "Add aaa to bbb" for adding software aaa to section bbb,
  "Remove aaa from bbb" for removing, "Fix license for aaa", etc.
